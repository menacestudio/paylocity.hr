﻿using System.Web.Mvc;

namespace Paylocity.HR.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}