/// <autosync enabled="true" />
/// <reference path="angular.min.js" />
/// <reference path="angular-animate.min.js" />
/// <reference path="angular-aria.min.js" />
/// <reference path="angular-material.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-resource.min.js" />
/// <reference path="app/paylocity.app.js" />
/// <reference path="app/paylocity.benefits.js" />
/// <reference path="app/paylocity.benefits.result-directive.js" />
/// <reference path="app/paylocity.benefits-controller.js" />
/// <reference path="app/paylocity.benefits-service.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="lodash.min.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
