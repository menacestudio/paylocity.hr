﻿(function () {
    "use strict";

    angular
        .module('app')
        .directive('benefitResults', benefitResultsDirective);

    benefitResultsDirective.$inject = [];
    function benefitResultsDirective() {
        return {
            require: 'ngModel',
            replace: true,
            restrict: 'E',
            scope: {
                amount: '='
            },
            templateUrl: '/Scripts/templates/paylocity.amount-template.html'
        };

    }

})();