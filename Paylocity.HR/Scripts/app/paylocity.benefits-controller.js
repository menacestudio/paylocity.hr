﻿(function () {
    "use strict";

    angular
        .module('app')
        .controller('benefitsController', benefitsController);

    benefitsController.$inject = ['benefitsService'];
    function benefitsController(benefitsService) {
        var vm = this;
        vm.amount = null;

        vm.employee = {
            name: 'Dennis Rongo',
            isEmployee: true
        };

        vm.dependents = [];

        /**
         * Add new dependent to the list.
         * @returns {} 
         */
        vm.addDependent = function() {
            vm.dependents.push({ name: '' });
        };

        /**
         * Calculate benefits and cost amount.
         * @returns {} 
         */
        vm.calculateBenefits = function () {
            vm.amount = benefitsService.calculateBenefits(vm.employee, vm.dependents);
        };

        /**
         * Evaluate input and remove item if empty.
         * @param {} index 
         * @param {} event 
         * @param {} dependent 
         * @returns {} 
         */
        vm.watchKey = function(index, event, dependent) {
            if (!dependent.name) {
                vm.dependents.splice(index, 1);
            }
        }

    }

})();