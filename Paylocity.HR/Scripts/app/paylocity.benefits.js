﻿var paylocity = window.paylocity || {};

paylocity.benefits = function () {
	var that = this;

	// Setup constants
	var payPerPeriod = 2000,
		paychecksPerYear = 26,
		percentDiscount = 0.1;

	var costPerEmployeePerYear = 1000,
		costPerDependentPerYear = 500;

	/**
	 * Calculation entry point and display calculation.
	 * @returns {} 
	 */
	var calculateBenefits = function (employee, dependents) {
	    return calculateCostByPeriods(employee, dependents);
	};

	/**
	 * Returns whether the person is discounted.
	 * @param {} person 
	 * @returns {} 
	 */
	var isDiscounted = function (person) {
	    if (person.name.toLowerCase().startsWith('a')) {
			return true;
		}

	    return false;
	}

	/**
	 * Calculates cost and discount per beneficiary per year.
	 * @param {} beneficiary 
	 * @returns {} 
	 */
	var calculateAmountPerPersonPerYear = function (beneficiary) {
	    var amountPerYear = {
	        cost: 0,
	        discount: 0
	    };

	    // Add business rules
	    if (!beneficiary.name) {
	        return amountPerYear;
	    };

	    if (beneficiary.isEmployee) {
		    amountPerYear.cost += costPerEmployeePerYear;
		} else {
		    amountPerYear.cost += costPerDependentPerYear;
		}

	    if (isDiscounted(beneficiary)) {
		    amountPerYear.discount = amountPerYear.cost * percentDiscount;
		    amountPerYear.cost = amountPerYear.cost - amountPerYear.discount;
		}

		return amountPerYear;
	}

	/**
	 * Calculates cost by different periods.
	 * @param {} employee 
	 * @param {} dependents 
	 * @returns {} 
	 */
	var calculateCostByPeriods = function (employee, dependents) {
		// Store the out-of-pocket cost per period
		var amount = {
			yearlyCost: 0,
			yearlyDiscount: 0,
			perPeriodCost: 0,
			payPeriodDiscount: 0,
			yearlyIncomeAfterDeduction: 0,
			payPeriodIncomeAfterDeduction: 0
		};

		// Calculate cost for employee
		var yearlyEmployeeAmount = calculateAmountPerPersonPerYear(employee);
		amount.yearlyCost = yearlyEmployeeAmount.cost;
	    amount.yearlyDiscount = yearlyEmployeeAmount.discount;

		// Calculate cost for dependents
		$.each(dependents, function (index, dependent) {
		    var costPerYear = calculateAmountPerPersonPerYear(dependent);
		    amount.yearlyCost += costPerYear.cost;
		    amount.yearlyDiscount += costPerYear.discount;
		});

		// Calculate cost and discount for each period
		amount.perPeriodCost = amount.yearlyCost / paychecksPerYear;
	    amount.payPeriodDiscount = amount.yearlyDiscount / paychecksPerYear;

	    // Calculate income
	    amount.yearlyIncomeAfterDeduction = (payPerPeriod * paychecksPerYear) - amount.yearlyCost;
	    amount.payPeriodIncomeAfterDeduction = amount.yearlyIncomeAfterDeduction / paychecksPerYear;

		return amount;
	};

	return {
		calculateBenefits: calculateBenefits
	}
}();