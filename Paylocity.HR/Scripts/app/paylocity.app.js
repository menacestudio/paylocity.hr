﻿(function () {
    "use strict";

    angular
        .module('app', ['ngResource', 'ngMaterial'])
        .config(function ($mdThemingProvider, $mdIconProvider) {
            $mdThemingProvider
                .theme('docs-dark', 'default')
                .primaryPalette('yellow')
                .dark();

        });
})();