﻿(function () {
    "use strict";

    angular
        .module('app')
        .service('benefitsService', benefitsService);

    benefitsService.$inject = ['$resource'];
    function benefitsService() {
        this.calculateBenefits = function(employee, dependents) {
            var benefitsByPeriod = paylocity.benefits.calculateBenefits(employee, dependents);
            return benefitsByPeriod;
        }
    }

})();